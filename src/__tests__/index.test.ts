import { asIs } from '../helpers/as-is';

describe('valid asis', () => {

  test('should return as is', () => {
    const input = 5;
    const result = asIs(input);
    expect(result).toEqual(input);
  });
});
